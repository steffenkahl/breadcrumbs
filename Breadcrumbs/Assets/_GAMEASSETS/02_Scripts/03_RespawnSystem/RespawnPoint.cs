﻿using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class RespawnPoint : MonoBehaviour
    {
        private Vector3 position;

        public Vector3 Position => position;
        
        private void Start()
        {
            position = transform.position;
            Message.Raise(new RegisterRespawnPointEvent(this));
        }
    }
}