﻿using System;
using System.Collections.Generic;
using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Breadcrumbs
{
    public class RespawnManager : MonoBehaviour
    {
        [SerializeField] private List<RespawnPoint> respawnPoints;
        [SerializeField] private Image screenOverlay;
        [SerializeField] private float fadeDuration;

        private void OnEnable()
        {
            Message<KillPlayerEvent>.Add(OnKillPlayerEvent);
            Message<RegisterRespawnPointEvent>.Add(OnRegisterRespawnPointEvent);
        }

        private void OnDisable()
        {
            Message<KillPlayerEvent>.Remove(OnKillPlayerEvent);
            Message<RegisterRespawnPointEvent>.Remove(OnRegisterRespawnPointEvent);
        }
        
        private void OnRegisterRespawnPointEvent(RegisterRespawnPointEvent ctx)
        {
            respawnPoints.Add(ctx.NewPoint);
        }

        private void OnKillPlayerEvent(KillPlayerEvent ctx)
        {
            screenOverlay.DOFade(1f, fadeDuration).OnComplete(() => RespawnPlayer(ctx.DeathPoint));
        }

        private void RespawnPlayer(Vector3 deathLocation)
        {
            Vector3 nearestRespawnPoint = GetNearestRespawnPoint(deathLocation).Position;
            Message.Raise(new PlayerTeleportEvent(nearestRespawnPoint));
            screenOverlay.DOFade(0f, fadeDuration);
            Message.Raise(new LeaveDeepForestEvent());
            Message.Raise(new PlayerRespawnedEvent(true, nearestRespawnPoint));
        }

        private RespawnPoint GetNearestRespawnPoint(Vector3 location)
        {
            RespawnPoint nearestPoint = null;
            float nearestDistance = Mathf.Infinity;
            foreach (RespawnPoint respawnPoint in respawnPoints)
            {
                if (Vector3.Distance(respawnPoint.Position, location) < nearestDistance)
                {
                    nearestPoint = respawnPoint;
                    nearestDistance = Vector3.Distance(respawnPoint.Position, location);
                }
            }
            return nearestPoint;
        }
    }
}