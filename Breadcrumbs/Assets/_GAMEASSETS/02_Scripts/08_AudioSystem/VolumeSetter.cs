﻿using System;
using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class VolumeSetter : MonoBehaviour
    {
        [SerializeField] private SoundType soundType;
        private AudioSource source;
        private Settings settings;

        private void OnEnable()
        {
            Message<ChangeVolumeEvent>.Add(OnChangeVolumeEvent);
        }
        
        private void OnDisable()
        {
            Message<ChangeVolumeEvent>.Remove(OnChangeVolumeEvent);
        }

        private void Start()
        {
            settings = GameObject.FindGameObjectWithTag(GameTags.SETTINGS).GetComponent<Settings>();
            source = GetComponent<AudioSource>();
            UpdateVolume();
        }

        private void OnChangeVolumeEvent(ChangeVolumeEvent ctx)
        {
            UpdateVolume();
        }

        private void UpdateVolume()
        {
            switch (soundType)
            {
                case SoundType.MAIN:
                    source.volume = settings.MainVolume * settings.MainVolume;
                    break;
                case SoundType.MUSIC:
                    source.volume = settings.MusicVolume * settings.MainVolume;
                    break;
                case SoundType.VOICE:
                    source.volume = settings.VoiceVolume * settings.MainVolume;
                    break;
                case SoundType.EFFECTS:
                    source.volume = settings.EffectsVolume * settings.MainVolume;
                    break;
            }
        }
    }
}