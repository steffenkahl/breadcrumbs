using UnityEngine;
using UnityEngine.Events;

namespace Breadcrumbs
{
    /// <summary>
    /// Script to change Music when entering the trigger. Works together with the Music Controller
    /// </summary>
    public class MusicTrigger : MonoBehaviour
    {
        [SerializeField] private AudioClip musicToPlay;
        public UnityEvent afterChangeEvent;
        private MusicController musicController;

        /// <summary>
        /// Gets triggered when entering a trigger collider with this script
        /// </summary>
        /// <param name="other">The collider that entered the Triggercollider</param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER)) //if the player collides with our invisible walls (this trigger) the Song will change
            {
                musicController = other.GetComponent<MusicController>();
                musicController.ChangeSong(musicToPlay);
                afterChangeEvent.Invoke();
            }
        }

        /// <summary>
        /// Can be called from other scripts to change the music
        /// </summary>
        public void TriggerChange()
        {
            musicController = GameObject.FindWithTag(GameTags.PLAYER).GetComponent<MusicController>();
            musicController.ChangeSong(musicToPlay);
            afterChangeEvent.Invoke();
        }
    }
}