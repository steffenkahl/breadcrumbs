﻿using System;
using Breadcrumbs;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.SceneManagement;

namespace _GAMEASSETS.Scripts
{
    public class PauseMenu : MonoBehaviour
    {
        [SerializeField] private bool isOpen;
        [SerializeField] private GameObject panel;
        [SerializeField] private string mainMenuScene;
        [SerializeField] private CameraController camController;
        [SerializeField] private AudioSource introSound;
        [SerializeField] private AudioSource midtroSound;
        [SerializeField] private AudioSource outroSound;
        [SerializeField] private bool isOutro;

        private bool introIsPlaying;
        private bool midtroIsPlaying;
        private bool outroIsPlaying;

        private void Update()
        {
            if (isOpen)
            {
                
                if (camController != null)
                {
                    Cursor.lockState = CursorLockMode.None;
                    camController.MouseIsLocked = false;
                }
                panel.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                if (!isOutro)
                {
                    if (camController != null)
                    {
                        Cursor.lockState = CursorLockMode.Locked;
                        camController.MouseIsLocked = true;
                    }
                }

                panel.SetActive(false);
                Time.timeScale = 1;
            }
        }

        public void OnBackToMenu()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(mainMenuScene);
        }

        public void ChangeMenu()
        {
            isOpen = !isOpen;

            if (isOpen)
            {
                if (introSound)
                {
                    if (introSound.isPlaying)
                    {
                        introIsPlaying = true;
                        introSound.Pause();
                    }
                }

                if (midtroSound)
                {
                    if (midtroSound.isPlaying)
                    {
                        midtroIsPlaying = true;
                        midtroSound.Pause();
                    }
                }

                if (outroSound)
                {
                    if (outroSound.isPlaying)
                    {
                        outroIsPlaying = true;
                        outroSound.Pause();
                    }
                }
            } else {
                if (introSound)
                {
                    if (introIsPlaying)
                    {
                        introSound.Play();
                    }
                }

                if (midtroSound)
                {
                    if (midtroIsPlaying)
                    {
                        midtroSound.Play();
                    }
                }

                if (outroSound)
                {
                    if (outroIsPlaying)
                    {
                        outroSound.Play();
                    }
                }
            }
        }
    }
}