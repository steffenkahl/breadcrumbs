﻿using Breadcrumbs.Events;
using UnityEngine;
using UnityEngine.Playables;

namespace Breadcrumbs
{
    public class TimelineWalkManager : MonoBehaviour
    {
        [SerializeField] private PlayableDirector walkingInForestTimeline;
        [SerializeField] private GameObject motherGO;
        [SerializeField] private float maxDistanceToKids;
        public GameObject player;
        private bool isWaiting;

        public bool IsWaiting
        {
            get => isWaiting;
            set => isWaiting = value;
        }

        private void Update()
        {
            if (isWaiting)
            {
                if (Vector3.Distance(player.transform.position, motherGO.transform.position) <= maxDistanceToKids)
                {
                    walkingInForestTimeline.Play();
                    isWaiting = false;
                }
                else
                {
                    walkingInForestTimeline.Pause();
                }
            }
        }

        public void TriggerSigns()
        {
            Message.Raise(new DisplaySignsEvent());
        }

        public void CheckForKids()
        {
            if (Vector3.Distance(player.transform.position, motherGO.transform.position) > maxDistanceToKids)
            {
                isWaiting = true;
            }
        }
    }
}