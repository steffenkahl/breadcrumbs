﻿using System;
using Breadcrumbs;
using UnityEngine;
using UnityEngine.Playables;

namespace _GAMEASSETS.Scripts
{
    public class TriggerTimeline : MonoBehaviour
    {
        [SerializeField] 
        private PlayableDirector timelineToPlay;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                timelineToPlay.Play();
            }
        }
    }
}