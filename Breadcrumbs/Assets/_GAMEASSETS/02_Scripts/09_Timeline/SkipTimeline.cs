using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.Playables;


public class SkipTimeline : MonoBehaviour
{
    [SerializeField] private PlayableDirector currentDirector;
    [SerializeField] private AudioSource backgroundAudio;
    [SerializeField] private bool holdsEasterEgg;
    [SerializeField] private AudioSource easterEggSound;
    [SerializeField] private UnityEvent afterSkip;
    [SerializeField] private bool isListening;

    public bool IsListening
    {
        get => isListening;
        set => isListening = value;
    }

    private void Start()
    {
        if (holdsEasterEgg)
        {
            easterEggSound.DOFade(1f, 0.2f).SetDelay(120f).OnComplete(PlayEasteregg);
        }
    }

    private void PlayEasteregg()
    {
        easterEggSound.Play();
    }

    public void SkipPerButton(InputAction.CallbackContext ctx)
    {
        if (isListening)
        {
            if (ctx.started)
            {
                SkipScene();
            }
        }
    }
    
    public void SkipScene()
    {
        currentDirector.time = currentDirector.duration;
        backgroundAudio.gameObject.SetActive(false);
        afterSkip.Invoke();
        isListening = false;
    }
}
