﻿using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Breadcrumbs
{
    public class TimelineTrigger : MonoBehaviour
    {
        [SerializeField] private PlayableDirector timeline;
        [SerializeField] private TimelineWalkManager timelineWalkManager;

        [SerializeField]
        private float neededTime;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                if (timeline.time < neededTime)
                {
                    timeline.time = neededTime;
                    timelineWalkManager.IsWaiting = false;
                    timeline.Play();
                }
            }
        }
    }
}