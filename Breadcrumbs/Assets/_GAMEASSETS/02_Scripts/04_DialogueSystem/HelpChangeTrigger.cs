﻿using System;
using Breadcrumbs;
using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class HelpChangeTrigger : MonoBehaviour
    {
        [SerializeField] private bool wasExecuted; //Checking weather or not it has already been executed
        [SerializeField] private HelpText helpText;

        private void OnTriggerEnter(Collider other) //Triggering Dialogue through walking through an invisible collider
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                if (!wasExecuted)
                {
                    ChangeText();
                }
            }
        }

        public void ChangeText()
        {
            if (helpText)
            {
                if (helpText.Tutorial)
                {
                    Message.Raise(new DisplayTutorialEvent(helpText));
                }
                else
                {
                    Message.Raise(new ChangeHelpTextEvent(helpText));
                }
            }
            wasExecuted = true;
        }
    }
}