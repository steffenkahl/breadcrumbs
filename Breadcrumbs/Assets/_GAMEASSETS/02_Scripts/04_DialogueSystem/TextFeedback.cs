﻿using Breadcrumbs;
using UnityEngine;

namespace Breadcrumbs
{
    [CreateAssetMenu(fileName = "Data", menuName = "Breadcrumbs/TextFeedback", order = 1)]
    public class TextFeedback : ScriptableObject
    {
        [SerializeField] private DialogueCharacterColor character;

        [SerializeField] [TextArea(3,10)]
        private string text; //Adding the displayed sentence the speaker says in the Unity Inspector

        [SerializeField] 
        private AudioClip soundFile;
        
        [Range(0.2f,10.0f)] [SerializeField] private float dialogueDisplayTime = 4.0f;

        public string Text => text;
        public AudioClip SoundFile => soundFile;
        public DialogueCharacterColor Character => character;
        public float DialogueDisplayTime => dialogueDisplayTime;
    }
}