﻿using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class DialogueTrigger : MonoBehaviour
    {
        [SerializeField] private bool isTriggered; //Checking weather or not the Dialogue has been triggered
        [SerializeField] private bool wasExecuted; //Checking weather or not it has already been executed
        [SerializeField] private int chapter; //Chapter 1-3
        [SerializeField] private HelpText helpText;
        [SerializeField] private bool useWrittenText;
        [SerializeField] private Dialogue[] writtenText;
        private int currentUsedDialogue = 0;

        private void OnTriggerEnter(Collider other) //Triggering Dialogue through walking through an invisible collider
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                if (!wasExecuted)
                {
                    if (isTriggered)
                    {
                        if (!useWrittenText)
                        {
                            Debug.Log("Dialogue-Queue was triggered at: " + transform.position);
                            NextDialogue();
                        }
                        else
                        {
                            Message.Raise(new ShowDialogueTextEvent(writtenText[currentUsedDialogue], this));
                            if (writtenText[currentUsedDialogue].ActionAfterDialogue == AfterDialogueActions.ACTIONANDNEXTDIALOGUE
                                || writtenText[currentUsedDialogue].ActionAfterDialogue == AfterDialogueActions.ACTION)
                            {
                                writtenText[currentUsedDialogue].afterDialogueAction.Invoke();
                            }
                            
                            if (helpText)
                            {
                                if (helpText.Tutorial)
                                {
                                    Message.Raise(new DisplayTutorialEvent(helpText));
                                }
                                else
                                {
                                    Message.Raise(new ChangeHelpTextEvent(helpText));
                                }
                            }
                                        
                            currentUsedDialogue += 1;
                            wasExecuted = true;
                        }
                    }
                }
            }
        }

        public void NextDialogue()
        {
            if (!wasExecuted)
            {
                Message.Raise(new ContinueDialogueEvent(chapter));
            }
            if (helpText)
            {
                if (helpText.Tutorial)
                {
                    Message.Raise(new DisplayTutorialEvent(helpText));
                }
                else
                {
                    Message.Raise(new ChangeHelpTextEvent(helpText));
                }
            }
            wasExecuted = true;
        }

        public void ShowNextWrittenDialogue(){
            Message.Raise(new ShowDialogueTextEvent(writtenText[currentUsedDialogue], this));
            if (writtenText[currentUsedDialogue].ActionAfterDialogue == AfterDialogueActions.ACTIONANDNEXTDIALOGUE
                || writtenText[currentUsedDialogue].ActionAfterDialogue == AfterDialogueActions.ACTION)
            {
                writtenText[currentUsedDialogue].afterDialogueAction.Invoke();
            }
            if (helpText)
            {
                if (helpText.Tutorial)
                {
                    Message.Raise(new DisplayTutorialEvent(helpText));
                }
                else
                {
                    Message.Raise(new ChangeHelpTextEvent(helpText));
                }
            }
            currentUsedDialogue += 1;
        }
    }
}