using System.Collections;
using System.Collections.Generic;
using Breadcrumbs;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class Dialogue 
{
    [SerializeField]
    private Characters speaker; //Adding the name of the current speaker in the Unity Inspector

    [SerializeField] [TextArea(3,10)]
    private string sentence; //Adding the displayed sentence the speaker says in the Unity Inspector

    [SerializeField] 
    private AudioClip soundFile;

    [SerializeField] private AfterDialogueActions actionAfterDialogue;

    [Range(0.2f,10.0f)] [SerializeField] private float dialogueDisplayTime = 4.0f;

    public UnityEvent afterDialogueAction;

    public Characters Speaker => speaker;
    public string Sentence => sentence;
    public AudioClip SoundFile => soundFile;
    public AfterDialogueActions ActionAfterDialogue => actionAfterDialogue;
    public float DialogueDisplayTime => dialogueDisplayTime;
}
