using System.Collections.Generic;
using Breadcrumbs.Events;
using DG.Tweening;
using UnityEngine;
using TMPro;

namespace Breadcrumbs
{
    public class DialogueManager : MonoBehaviour
    {
        private List<Dialogue[]> dialogueList = new List<Dialogue[]>(); //Making a List of Dialogue Arrays

        [SerializeField] private List<DialogueCharacterColor> characterColors;

        /*Since the player will be able to skip a part of the dialogue 
         * we added divided the story into chapters and
         * made them seperate arrays. ChapterOne has to be played, 
         * ChapterTwo is skippable and ChapterThree is the ending, wheather or not
         * the second chapter is skipped*/
        [SerializeField] private Dialogue[] chapterOne;
        [SerializeField] private Dialogue[] chapterTwo;
        [SerializeField] private Dialogue[] chapterThree;

        [SerializeField] private TMP_Text dialogueText; //drag and drop the TMPText that should display the current dialogue sentence

        [SerializeField] private TMP_Text dialogueName; //drag and drop the TMPText that should display the current character

        [SerializeField] private AudioSource audioSource; // Source for text to speech

        [SerializeField] private float dialogueDisplayTime;

        private Settings settings;
        private int currentChapter;
        private int currentDialogue;


        private void OnEnable() //subscribing to the Event that triggers the current Dialogue[i] to be played
        {
            Message<ContinueDialogueEvent>.Add(OnContinueDialogue);
            Message<ShowTextFeedbackEvent>.Add(OnShowTextFeedbackEvent);
            Message<ShowDialogueTextEvent>.Add(OnShowDialogueTextEvent);
        }

        private void OnDisable() //unsubscribing to the Event that triggers the current Dialogue[i] to be played
        {
            Message<ContinueDialogueEvent>.Remove(OnContinueDialogue);
            Message<ShowTextFeedbackEvent>.Remove(OnShowTextFeedbackEvent);
            Message<ShowDialogueTextEvent>.Remove(OnShowDialogueTextEvent);
        }

        //At start load all queued dialogues for the three Acts
        private void Start() //Adding the Chapters to the List 
        {
            settings = GameObject.FindGameObjectWithTag(GameTags.SETTINGS).GetComponent<Settings>();
            dialogueList.Add(chapterOne);
            dialogueList.Add(chapterTwo);
            dialogueList.Add(chapterThree);
            currentChapter = 0;
            currentDialogue = 0;
            dialogueName.text = "";
            dialogueText.text = "";
        }

        //Used for queued dialogues to show next Dialogue
        private void OnContinueDialogue(ContinueDialogueEvent ctx) //See "DialogueTrigger"-Script for reference
        {
            ContinueDialogue(ctx.Chapter);
        }

        private void OnShowDialogueTextEvent(ShowDialogueTextEvent ctx){
            //First fade in the ui for the dialogue via DOTween
            dialogueText.DOFade(1f, 0.2f);
            dialogueName.DOFade(1f, 0.2f);
            
            dialogueDisplayTime = ctx.ShownDialogue.DialogueDisplayTime;
            
            //If the character has a set color and name, then use it:
            if (TryGetCharacterColor(ctx.ShownDialogue.Speaker) != null)
            {
                DialogueCharacterColor characterColor = TryGetCharacterColor(ctx.ShownDialogue.Speaker);
                dialogueName.text = characterColor.CharacterName;
                dialogueName.color = characterColor.CharacterColor;
            }

            dialogueText.text = ctx.ShownDialogue.Sentence;

            //If the next shown dialogue has a soundfile, play it:
            if (ctx.ShownDialogue.SoundFile)
            {
                audioSource.volume = settings.VoiceVolume * settings.MainVolume;
                audioSource.clip = ctx.ShownDialogue.SoundFile;
                audioSource.Play();
            }

            //If the AfterDialogueAction is NextDialogue or ActionAndNextDialogue, then the next dialogue should load too
            if (ctx.ShownDialogue.ActionAfterDialogue == AfterDialogueActions.NEXTDIALOGUE 
                || ctx.ShownDialogue.ActionAfterDialogue == AfterDialogueActions.ACTIONANDNEXTDIALOGUE)
            {
                dialogueText.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime).OnComplete(() => { ctx.UsedTrigger.ShowNextWrittenDialogue(); });
                dialogueName.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
            }

            dialogueText.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
            dialogueName.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
        }

        //Used for written dialogue that sits directly in a trigger and is not queued global:
        public void OnShowTextFeedbackEvent(ShowTextFeedbackEvent ctx)
        {
            dialogueDisplayTime = ctx.Feedback.DialogueDisplayTime;
            dialogueText.DOFade(1f, 0.2f);
            dialogueName.DOFade(1f, 0.2f);

            dialogueName.text = ctx.Feedback.Character.CharacterName;
            dialogueName.color = ctx.Feedback.Character.CharacterColor;
            dialogueText.text = ctx.Feedback.Text;
            
            if (ctx.Feedback.SoundFile)
            {
                audioSource.clip = ctx.Feedback.SoundFile;
                audioSource.Play();
            }
            
            dialogueText.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
            dialogueName.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
        }

        //Used for queued Dialogues that resent global in the DialogueManager:
        private void ContinueDialogue(int chapter)
        {
            if (chapter - 1 != currentChapter)
            {
                currentDialogue = 0;
            }
            currentChapter = chapter - 1;
            dialogueText.DOFade(1f, 0.2f);
            dialogueName.DOFade(1f, 0.2f);
            dialogueDisplayTime = dialogueList[currentChapter][currentDialogue].DialogueDisplayTime;
            Debug.Log("Continue Dialogue in Chapter " + chapter);

            if (TryGetCharacterColor(dialogueList[currentChapter][currentDialogue].Speaker) != null)
            {
                DialogueCharacterColor characterColor = TryGetCharacterColor(dialogueList[currentChapter][currentDialogue].Speaker);
                dialogueName.text = characterColor.CharacterName;
                dialogueName.color = characterColor.CharacterColor;
            }

            dialogueText.text = dialogueList[currentChapter][currentDialogue].Sentence;

            if (dialogueList[currentChapter][currentDialogue].SoundFile)
            {
                audioSource.clip = dialogueList[currentChapter][currentDialogue].SoundFile;
                audioSource.Play();
            }

            if (dialogueList[currentChapter][currentDialogue].ActionAfterDialogue == AfterDialogueActions.NEXTDIALOGUE)
            {
                if (currentDialogue < dialogueList[currentChapter].Length)
                {
                    currentDialogue += 1;
                }
                else if (currentChapter < dialogueList.Count)
                {
                    currentChapter += 1;
                    currentDialogue = 0;
                }

                dialogueText.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime).OnComplete(() => { ContinueDialogue(chapter); });
                dialogueName.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
            }
            else if (dialogueList[currentChapter][currentDialogue].ActionAfterDialogue == AfterDialogueActions.NEXTCHAPTER)
            {
                if (currentDialogue < dialogueList[currentChapter].Length)
                {
                    currentDialogue += 1;
                }
                else if (currentChapter < dialogueList.Count)
                {
                    currentChapter += 1;
                    currentDialogue = 0;
                }

                dialogueText.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime).OnComplete(() => { ContinueDialogue(chapter + 1); });
                dialogueName.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
            }
            else
            {
                if (currentDialogue < dialogueList[currentChapter].Length)
                {
                    currentDialogue += 1;
                }
                else if (currentChapter < dialogueList.Count)
                {
                    currentChapter += 1;
                    currentDialogue = 0;
                }

                dialogueText.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
                dialogueName.DOFade(0f, 0.2f).SetDelay(dialogueDisplayTime);
            }
        }

        /// <summary>
        /// Tries getting the charactercolor for a specific character that should have dialogue. Returns null if color is not set for character.
        /// </summary>
        /// <param name="character">Speaking Character</param>
        /// <returns>DialogueCharacterColor of the speaking character</returns>
        private DialogueCharacterColor TryGetCharacterColor(Characters character)
        {
            //Looking through all DialogueCharacterColors in the List to find the needed Character:
            foreach (DialogueCharacterColor col in characterColors)
            {
                if (col.Character == character)
                {
                    return col; //return found charactercolor
                }
            }

            return null; //return null if no correct charactercolor has been found
        }
    }
}