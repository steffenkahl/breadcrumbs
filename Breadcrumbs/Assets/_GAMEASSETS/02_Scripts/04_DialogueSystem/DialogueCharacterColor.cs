﻿using System;
using Breadcrumbs;
using UnityEngine;

namespace Breadcrumbs
{
    [Serializable]
    public class DialogueCharacterColor
    {
        [SerializeField] private Characters character;
        [SerializeField] private string characterName;
        [SerializeField] private Color characterColor = Color.white;

        public Characters Character => character;
        public string CharacterName => characterName;
        public Color CharacterColor => characterColor;
    }
}