using System;
using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class DestroyPebble : MonoBehaviour
    {
        private void OnEnable()
        {
            Message<DestroyPebblesEvent>.Add(OnDestroyPebblesEvent);
        }

        private void OnDisable()
        {
            Message<DestroyPebblesEvent>.Remove(OnDestroyPebblesEvent);
        }

        private void OnDestroyPebblesEvent(DestroyPebblesEvent ctx)
        {
            Destroy(gameObject);
        }
    }
}