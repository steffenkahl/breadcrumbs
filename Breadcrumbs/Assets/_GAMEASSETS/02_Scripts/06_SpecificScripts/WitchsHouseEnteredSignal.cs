using UnityEngine;
using UnityEngine.Events;

public class WitchsHouseEnteredSignal : MonoBehaviour
{
    [SerializeField] private UnityEvent thingsToDo;

    public void Trigger()
    {
        thingsToDo.Invoke();
    }
}
