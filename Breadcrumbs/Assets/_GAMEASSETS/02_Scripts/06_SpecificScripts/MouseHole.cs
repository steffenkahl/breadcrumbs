
using UnityEngine;

namespace Breadcrumbs
{
    public class MouseHole : MonoBehaviour
    {
        [SerializeField] private GameObject itemToActivate;

        public void ActivateItem()
        {
            itemToActivate.SetActive(true);
        }
    }
}