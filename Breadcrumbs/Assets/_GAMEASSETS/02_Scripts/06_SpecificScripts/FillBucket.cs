using System;
using System.Collections;
using System.Collections.Generic;
using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class FillBucket : MonoBehaviour
    {
        [SerializeField] private Item emptyBucket;
        [SerializeField] private Item filledBucket;
        private PlayerHands lastUsedHand;
        private ActingCharacters lastUsedCharacter;

        private void OnEnable()
        {
            Message<ItemPickupEvent>.Add(OnItemPickupEvent);
        }
        
        private void OnDisable()
        {
            Message<ItemPickupEvent>.Remove(OnItemPickupEvent);
        }

        private void OnItemPickupEvent(ItemPickupEvent ctx)
        {
            if (ctx.PickedUpItem == emptyBucket)
            {
                lastUsedHand = ctx.UsedHand;
            }
        }
        
        public void OnInteracted()
        {
            Message.Raise(new ItemPickupEvent(filledBucket, lastUsedHand, ActingCharacters.GRETEL));
            Debug.Log("Bucket filled");
        }

        private void Start()
        {
            lastUsedHand = PlayerHands.LEFT;
        }
    }
}