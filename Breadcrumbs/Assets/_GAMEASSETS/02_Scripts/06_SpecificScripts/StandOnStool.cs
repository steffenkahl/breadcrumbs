
using UnityEngine;

namespace Breadcrumbs
{
    public class StandOnStool : MonoBehaviour
    {
        [SerializeField] private InteractableHolder holderToActivate;

        private void OnTriggerEnter(Collider other) {
            if(other.CompareTag(GameTags.PLAYER)){
                holderToActivate.IsAvailable = true;
            }
        }

        private void OnTriggerExit(Collider other) {
            if(other.CompareTag(GameTags.PLAYER)){
                holderToActivate.IsAvailable = false;
            }
        }
    }
}