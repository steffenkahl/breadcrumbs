using System.Collections;
using System.Collections.Generic;
using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.UI;


namespace Breadcrumbs
{ 
public class Sleep : MonoBehaviour
{
        [SerializeField]
        private Image blackScreen;

        [SerializeField]
        private float fadeTime;

        [SerializeField] 
        private float eveningTime;

        [SerializeField] 
        private GameObject motherGO;

        [SerializeField] 
        private UnityEvent afterSleeping;

       public void OnActivation()
        {
            blackScreen.DOFade(1f, fadeTime).OnComplete(OnFadeComplete);
        }

        private void OnFadeComplete()
        {
            blackScreen.DOFade(0f, fadeTime);
            Message.Raise(new ChangeTimeEvent(eveningTime));
            motherGO.SetActive(true);
            afterSleeping.Invoke();
        }
}
}
