using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Breadcrumbs;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

namespace Breadcrumbs
{ 
/// <summary>
/// Script for the first part of the game where you need to help your sibling going through the window
/// </summary>
public class HelpSiblingThroughWindow : MonoBehaviour
{
    //References to different GameObjects well need:
    [SerializeField] private GameObject hanselGO;
    [SerializeField] private GameObject gretelGO;
    [SerializeField] private GameObject targetPosition;
    [SerializeField] private GameObject targetMiddlePosition;
    private InteractableHolder interactableHolder;
    private GameObject followingSibling; //Will hold the following sibling
    private GameObject playingSibling; //Will hold the currently played sibling 
    private bool helpedSibling; //If the sibling was already helped to prevent double helping

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GameTags.PLAYER))
        {
            //When the player gets into range of the window, the sibling should come closer
            UpdateRoles();
            if (!helpedSibling)
            {
                followingSibling.GetComponent<FollowingPlayer>().FollowsPlayer = false;
                playingSibling.GetComponent<FollowingPlayer>().FollowsPlayer = false;
                followingSibling.transform.DOMove(transform.position, 1f);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //When the player gets out of range of the window, the sibling should follow the player again unless the player activates the window
        UpdateRoles();
        if (other.gameObject.CompareTag(GameTags.PLAYER))
        {
            if (!helpedSibling)
            {
                followingSibling.GetComponent<FollowingPlayer>().FollowsPlayer = true;
                playingSibling.GetComponent<FollowingPlayer>().FollowsPlayer = true;
            }
        }
    }

    private void Start()
    {
        //Get references and check if the siblings were added to the script
        interactableHolder = GetComponent<InteractableHolder>();
        if (hanselGO == null || gretelGO == null)
        {
            Debug.LogWarning("Sibling-References missing!");
            Destroy(this);
        }
        UpdateRoles();
    }
    
    private void UpdateRoles()
    {
        //Update which sibling is playing and which one is following
        if (GameManager.IsPlayingHansel)
        {
            followingSibling = gretelGO;
            playingSibling = hanselGO;
        }
        else
        {
            followingSibling = hanselGO;
            playingSibling = gretelGO;
        }
    }

    public void HelpSibling()
    {
        //Gets activated from the interactable to start helping
        //Add all Animation Code needed later here!
        Debug.Log("Helps Sibling");
        if (!helpedSibling)
        {
            GameManager.Instance.CanPlayGretel = false;
            GameManager.Instance.CanPlayHansel = false;
            followingSibling.transform.DOMove(targetMiddlePosition.transform.position, 1f).OnComplete(OnMiddleHelp);
            Debug.Log("Helped Sibling through window");
            interactableHolder.WasActivated = true;
        }
    }

    private void OnMiddleHelp()
    {
        followingSibling.transform.DOMove(targetPosition.transform.position, 1f).OnComplete(OnCompletedHelp);
    }

    private void OnCompletedHelp()
    {
        //After the help is completed, both players should be playable again, not follow each other and
        //the variable helpedSibling should be true from now on
        GameManager.Instance.CanPlayGretel = true;
        GameManager.Instance.CanPlayHansel = true;
        helpedSibling = true;
        followingSibling.GetComponent<FollowingPlayer>().FollowsPlayer = false;
        playingSibling.GetComponent<FollowingPlayer>().FollowsPlayer = false;
        Destroy(gameObject);
    }
}
}
