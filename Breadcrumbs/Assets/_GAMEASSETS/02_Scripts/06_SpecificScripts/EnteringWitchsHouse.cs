using System.Collections;
using System.Collections.Generic;
using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace Breadcrumbs
{ 
public class EnteringWitchsHouse : MonoBehaviour
{
        [SerializeField]
        private Image blackScreen;
        
        [SerializeField]
        private Image whiteScreen;

        [SerializeField]
        private float fadeTime;

        [SerializeField] 
        private float morningTime;

        [SerializeField] private GameObject hanselPosition;
        [SerializeField] private GameObject gretelPosition;
        [SerializeField] private GameObject hansel;
        [SerializeField] private GameObject gretel;

        [SerializeField] private string sceneToLoadAfterFinish;

        [SerializeField] 
        private UnityEvent afterSleeping;
        
        private GameObject followingSibling; //Will hold the following sibling
        private GameObject playingSibling; //Will hold the currently played sibling 

       public void OnActivation()
        {
            blackScreen.DOFade(1f, fadeTime).OnComplete(OnFadeComplete);
        }

        private void OnFadeComplete()
        {
            Debug.Log("On Fade Complete");
            blackScreen.DOFade(0f, fadeTime);
            Message.Raise(new ChangeTimeEvent(morningTime));
            
            hansel.GetComponent<FollowingPlayer>().FollowsPlayer = false;
            gretel.GetComponent<FollowingPlayer>().FollowsPlayer = false;
            
            afterSleeping.Invoke();
            
            if (GameManager.IsPlayingHansel)
            {
                Message.Raise(new PlayerTeleportEvent(hanselPosition.transform.position));
                gretel.transform.position = gretelPosition.transform.position;
            }
            else
            {
                Message.Raise(new PlayerTeleportEvent(gretelPosition.transform.position));
                hansel.transform.position = hanselPosition.transform.position;
            }
        }

        public void OnEscapingWitch()
        {
            whiteScreen.DOFade(1f, fadeTime).OnComplete(OnEscapeComplete);
        }

        private void OnEscapeComplete()
        {
            if (sceneToLoadAfterFinish != null)
            {
                SceneManager.LoadScene(sceneToLoadAfterFinish);
            }
        }
    }
}
