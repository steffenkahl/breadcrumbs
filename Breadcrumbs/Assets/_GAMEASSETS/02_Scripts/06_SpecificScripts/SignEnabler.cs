using Breadcrumbs.Events;
using UnityEngine;

public class SignEnabler : MonoBehaviour
{
    [SerializeField] private GameObject signModel;
    private void OnEnable()
    {
        Message<DisplaySignsEvent>.Add(OnDisplaySignsEvent);
    }

    private void OnDisable()
    {
        Message<DisplaySignsEvent>.Remove(OnDisplaySignsEvent);
    }

    private void Start()
    {
        signModel.SetActive(false);
    }

    private void OnDisplaySignsEvent(DisplaySignsEvent ctx)
    {
        signModel.SetActive(true);
    }
}
