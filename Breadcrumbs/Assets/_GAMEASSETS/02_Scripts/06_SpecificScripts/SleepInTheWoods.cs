using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Breadcrumbs
{ 
    public class SleepInTheWoods : MonoBehaviour
    {
        [SerializeField]
        private Image blackScreen;

        [SerializeField]
        private float fadeTime;

        [SerializeField] 
        private float morningTime;

        [SerializeField] 
        private UnityEvent afterSleeping;

       public void OnActivation()
        {
            blackScreen.DOFade(1f, fadeTime).OnComplete(OnFadeComplete);
        }

        private void OnFadeComplete()
        {
            blackScreen.DOFade(0f, fadeTime);
            Message.Raise(new ChangeTimeEvent(morningTime));
            afterSleeping.Invoke();
            Message.Raise(new DestroyPebblesEvent());
        }
    }
}
