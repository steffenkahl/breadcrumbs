using UnityEngine;
using UnityEngine.UI;//require to access the Slider
using TMPro;

namespace Breadcrumbs
{
    public class SetSlider : MonoBehaviour
    {
        //serialized to be able to drag and drop the Text in the unity inspector
        [SerializeField] private Slider mainVolumeSlider;
        [SerializeField] private Slider musicVolumeSlider;
        [SerializeField] private Slider voiceVolumeSlider;
        [SerializeField] private Slider effectsVolumeSlider;
        
        [SerializeField] private TextMeshProUGUI tmpVolumeMain;
        [SerializeField] private TextMeshProUGUI tmpVolumeMusic;
        [SerializeField] private TextMeshProUGUI tmpVolumeVoice;
        [SerializeField] private TextMeshProUGUI tmpVolumeEffects;

        [SerializeField] private TMP_Dropdown qualityDropdown;

        private Settings settings;
        
        private void Start()
        {
            settings = GameObject.FindGameObjectWithTag(GameTags.SETTINGS).GetComponent<Settings>();
            mainVolumeSlider.value = PlayerPrefs.GetFloat("MainVolume", settings.MainVolume);
            musicVolumeSlider.value = PlayerPrefs.GetFloat("MusicVolume", settings.MusicVolume);
            voiceVolumeSlider.value = PlayerPrefs.GetFloat("VoiceVolume", settings.VoiceVolume);
            effectsVolumeSlider.value = PlayerPrefs.GetFloat("EffectsVolume", settings.EffectsVolume);
            qualityDropdown.value = PlayerPrefs.GetInt("QualityLevel", 4);
            
            ShowSliderValue();
        }

        public void SetVolumeLevel(string soundType)
        {
            switch (soundType)
            {
                case "Main":
                    settings.SetVolume(SoundType.MAIN, mainVolumeSlider.value);
                    break;
                case "Music":
                    settings.SetVolume(SoundType.MUSIC, musicVolumeSlider.value);
                    break;
                case "Voice":
                    settings.SetVolume(SoundType.VOICE, voiceVolumeSlider.value);
                    break;
                case "Effects":
                    settings.SetVolume(SoundType.EFFECTS, effectsVolumeSlider.value);
                    break;
            }
            ShowSliderValue();
        }

        public void SetQualityLevel()
        {
            QualitySettings.SetQualityLevel(qualityDropdown.value);
            PlayerPrefs.SetInt("QualityLevel", qualityDropdown.value);
        }

        public void ShowSliderValue()
        {
            tmpVolumeMain.text = "Gesamt: " + (int) (mainVolumeSlider.value * 100) + "%"; //to show the float value in an int number
            tmpVolumeMusic.text = "Musik: " + (int) (musicVolumeSlider.value * 100) + "%"; //to show the float value in an int number
            tmpVolumeVoice.text = "Sprache: " + (int) (voiceVolumeSlider.value * 100) + "%"; //to show the float value in an int number
            tmpVolumeEffects.text = "Effekte: " + (int) (effectsVolumeSlider.value * 100) + "%"; //to show the float value in an int number
        }
    }
}