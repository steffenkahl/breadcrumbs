using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class Settings : MonoBehaviour
    {
        //two variables for the values we want the player to be able to set themselves when playing the game
        [Range(0f,1f)][SerializeField] private float mainVolume; //For the main Volume
        [Range(0f,1f)][SerializeField] private float musicVolume; //For the music Volume
        [Range(0f,1f)][SerializeField] private float voiceVolume; //For the voice Volume
        [Range(0f,1f)][SerializeField] private float effectsVolume; //For the effects Volume

        public float MainVolume
        {
            get => mainVolume;
            set => mainVolume = value;
        }

        public float MusicVolume
        {
            get => musicVolume;
            set => musicVolume = value;
        }

        public float VoiceVolume
        {
            get => voiceVolume;
            set => voiceVolume = value;
        }

        public float EffectsVolume
        {
            get => effectsVolume;
            set => effectsVolume = value;
        }

        private void Awake()
        { 
            if (GameObject.FindGameObjectWithTag(GameTags.SETTINGS) != gameObject)
            { 
                Destroy(gameObject);
            }//We have to make sure this is the only instance of this script to make sure no other script gets the wrong variables

            DontDestroyOnLoad(gameObject);
            
            /*We save the new values of the Volume and the Sensitivity in the PlayerPrefs when the Player changes them in the Settings 
            * (the Code used for this is "SetSlider")
            * When the Game is started, we get the saved Values from the PlayerPrefabs and assign them to our Variables*/
            mainVolume = PlayerPrefs.GetFloat("MainVolume", 0.65f);
            musicVolume = PlayerPrefs.GetFloat("MusicVolume", 1f);
            voiceVolume = PlayerPrefs.GetFloat("VoiceVolume", 1f);
            effectsVolume = PlayerPrefs.GetFloat("EffectsVolume", 1f);
            //The MouseSensitivity is 0.65f as default, because 1.0f is too high, the player has the possibility to increase it (and decrease of course)
        }

        public void SetVolume(SoundType soundType, float value)
        {
            switch (soundType)
            {
                case SoundType.MAIN:
                    mainVolume = value;
                    PlayerPrefs.SetFloat("MainVolume", value);
                    break;
                case SoundType.MUSIC:
                    musicVolume = value;
                    PlayerPrefs.SetFloat("MusicVolume", value);
                    break;
                case SoundType.VOICE:
                    voiceVolume = value;
                    PlayerPrefs.SetFloat("VoiceVolume", value);
                    break;
                case SoundType.EFFECTS:
                    effectsVolume = value;
                    PlayerPrefs.SetFloat("EffectsVolume", value);
                    break;
            }
            Message.Raise(new ChangeVolumeEvent());
        }
    }
}