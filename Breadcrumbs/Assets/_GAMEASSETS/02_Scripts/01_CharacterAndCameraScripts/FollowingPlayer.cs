using System;
using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class FollowingPlayer : MonoBehaviour
    {
        [SerializeField] private float speed; //the speed in which the player is being followed
        [SerializeField] private float rotateSpeed;
        [SerializeField] private bool followsPlayer;

        [SerializeField] private float teleportDistance = 30f;
        [SerializeField] private float maxDistanceToFollow = 5f;
        [SerializeField] private float minDistanceToFollow = 2.5f;

        private Transform player; //later storing the position of the Player-GO in this Variable
        private CharacterControllerMovement playerMover;

        public bool FollowsPlayer
        {
            get => followsPlayer;
            set => followsPlayer = value;
        }
        
        /*When the player switches the characters the following has to happen:
         * The Player-Tag has to be removed from the Go the Player is NOT playing and added to the other,
         * The Script has to be "deactivated" in the Go is Player is in and "activated" in the other,
         * Because we need it to call Start and look for whom to follow again*/
        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
            playerMover = player.GetComponent<CharacterControllerMovement>();
        }

        private void OnEnable()
        {
            Message<PlayerRespawnedEvent>.Add(OnPlayerRespawnedEvent);
        }

        private void OnDisable()
        {
            Message<PlayerRespawnedEvent>.Remove(OnPlayerRespawnedEvent);
        }

        private void OnPlayerRespawnedEvent(PlayerRespawnedEvent ctx)
        {
            transform.position = ctx.RespawnPosition + 3* Vector3.up; //Offset the respawn position by a bit so the characters dont merge
        }

        private void Update()
        {
            if (followsPlayer)
            {
                float distance = Vector3.Distance(transform.position, player.position);
                if (distance > minDistanceToFollow && distance < maxDistanceToFollow)
                {
                    transform.position = Vector3.MoveTowards(transform.position, player.position, distance * speed * Time.deltaTime);
                }
                else if(distance > teleportDistance){
                    transform.position = player.position + 3* Vector3.up;
                }

                if (transform.position.y < -50)
                {
                    transform.position += 100 * Vector3.up;
                }
            }

            Quaternion rotation = Quaternion.LookRotation(player.transform.position - transform.position);
            rotation = Quaternion.Euler(new Vector3(0f, rotation.eulerAngles.y, 0f));
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotateSpeed);
            
        }
        
    }
}