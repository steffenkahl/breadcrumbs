﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace Breadcrumbs
{
    /// <summary>
    /// Helper for finding the current Plattform and working with it
    /// </summary>
    public class PlatformHelper : MonoBehaviour
    {
        private static DeviceType activeDeviceType;
        private static PlayModes activePlayMode;
        [SerializeField] private PlayModes standardPlayMode;
        [SerializeField] private GameObject VRRig;
        [SerializeField] private GameObject WebGLRig;
        [SerializeField] private GameObject DesktopRig;

        /// <summary>
        /// The currently used DeviceType detected by the PlatformHelper
        /// </summary>
        public static DeviceType ActiveDeviceType => activeDeviceType;

        /// <summary>
        /// The currently used Playmode selected by User or Headset
        /// </summary>
        public static PlayModes ActivePlayMode => activePlayMode;
        
        //Check in Awake which Device the User uses:
        private void Awake()
        {
            activePlayMode = standardPlayMode;
            
            //Check for VR
            if (isVRPresent())
            {
                activeDeviceType = DeviceType.VR;
                activePlayMode = PlayModes.SINGLEPLAYER_VR;
                VRRig.SetActive(true);
                WebGLRig.SetActive(false);
                DesktopRig.SetActive(false);
            }

            //Check for WebGL
            if (Application.platform == RuntimePlatform.WebGLPlayer)
            {
                activeDeviceType = DeviceType.WEBGL;
                VRRig.SetActive(false);
                WebGLRig.SetActive(true);
                DesktopRig.SetActive(false);
            }
            
            //Check for Desktop
            if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                activeDeviceType = DeviceType.DESKTOP;
                VRRig.SetActive(false);
                WebGLRig.SetActive(false);
                DesktopRig.SetActive(true);
            }
            
            //Check for Editor
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
            {
                activeDeviceType = DeviceType.EDITOR;
                VRRig.SetActive(false);
                WebGLRig.SetActive(false);
                DesktopRig.SetActive(true);
            }
        }

        /// <summary>
        /// Switches between Splitscreen-Coop and Singleplayer if possible
        /// </summary>
        /// <returns>(bool) if switching was successful</returns>
        public static bool SwitchCoop()
        {
            if (activePlayMode == PlayModes.SINGLEPLAYER)
            {
                activePlayMode = PlayModes.SPLITSCREEN_COOP;
            }
            else if (activePlayMode == PlayModes.SPLITSCREEN_COOP)
            {
                activePlayMode = PlayModes.SINGLEPLAYER;
            }
            else
            {
                return false;
            }
            return true;
        }
        
        /// <summary>
        /// Checks if the user has a VR-Headset connected
        /// </summary>
        /// <returns>(bool) if a Headset is connected</returns>
        public static bool isVRPresent()
        {
            var xrDisplaySubsystems = new List<XRDisplaySubsystem>();
            SubsystemManager.GetInstances<XRDisplaySubsystem>(xrDisplaySubsystems);
            foreach (var xrDisplay in xrDisplaySubsystems)
            {
                if (xrDisplay.running)
                {
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// Types of possible Devices
    /// </summary>
    public enum DeviceType
    {
        NONE,
        DESKTOP,
        EDITOR,
        WEBGL,
        VR
    }

    /// <summary>
    /// Types of possible PlayModes
    /// </summary>
    public enum PlayModes
    {
        NONE,
        SINGLEPLAYER_VR,
        SINGLEPLAYER,
        SPLITSCREEN_COOP
    }
}