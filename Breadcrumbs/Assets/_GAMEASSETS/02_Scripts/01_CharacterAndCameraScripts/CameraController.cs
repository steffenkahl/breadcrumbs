using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Breadcrumbs
{
    public class CameraController : MonoBehaviour
    {
        //Controller for the main Camera of the Player

        [SerializeField] private bool mouseIsLocked;
        private CinemachineVirtualCamera vCam;
        private Settings settings; //Reference to the settings for the mouseSensitivity

        public bool MouseIsLocked
        {
            get => mouseIsLocked;
            set => mouseIsLocked = value;
        }
        
        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            if (mouseIsLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Confined;
            }
        }


        /*[SerializeField] private float sensitivityModifierX; //A Modifier for the sensitivity in X Axis
        [SerializeField] private float sensitivityModifierY; //A Modifier for the sensitivity in Y Axis
        
        private Vector2 rawLookingAxis; //The raw input for movement from the new input system
    
        private CharacterControllerMovement characterControllerMovement; //Reference to the player movement Controller
        
    
    
        
    
        private void LateUpdate()
        {
            float mouseX = (rawLookingAxis.x * mouseSensitivity) * sensitivityModifierX;
            float mouseY = -(rawLookingAxis.y * mouseSensitivity) * sensitivityModifierY;
        }
    
        public void OnLooking(InputAction.CallbackContext value)
        {
    
            //Get the raw Axis Values for Looking
            rawLookingAxis = value.ReadValue<Vector2>(); //Reads the Values of the mouse or right joystick
            Debug.Log(message: "Player is looking around");
           
        }
    
        public void UpdateSensitivity()
        {
            mouseSensitivity = settings.MouseSensitivity;
        }*/

    }
}