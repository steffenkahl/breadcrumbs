using System;
using Breadcrumbs;
using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using UnityEngine;

public class SwitchCharacterManager : MonoBehaviour
{
    [SerializeField] private GameObject hanselGO;
    [SerializeField] private GameObject gretelGO;
    private GameObject playerGO;
    private GameObject followingSibling;
    private GameObject playingSibling;

    private void OnEnable()
    {
        Message<CharacterSwitchSuccessEvent>.Add(OnCharacterSwitchSuccessEvent);
    }
    
    private void OnDisable()
    {
        Message<CharacterSwitchSuccessEvent>.Remove(OnCharacterSwitchSuccessEvent);
    }

    private void Start()
    {
        if (hanselGO == null || gretelGO == null)
        {
            Debug.LogWarning("SwitchCharacterManager doesnt have siblings!");
            Destroy(this);
        }
        playerGO = GameObject.FindWithTag(GameTags.PLAYER);
        UpdateVisibilities();
    }

    // Update is called once per frame
    void Update()
    {
        playingSibling.transform.position = playerGO.transform.position;
    }

    private void OnCharacterSwitchSuccessEvent(CharacterSwitchSuccessEvent ctx)
    {
        Message.Raise(new PlayerTeleportEvent(followingSibling.transform.position));
        UpdateVisibilities();
    }

    private void UpdateVisibilities()
    {
        if (GameManager.IsPlayingHansel)
        {
            followingSibling = gretelGO;
            playingSibling = hanselGO;
        }
        else
        {
            followingSibling = hanselGO;
            playingSibling = gretelGO;
        }

        followingSibling.SetActive(true);
        playingSibling.SetActive(false);
    }
}
