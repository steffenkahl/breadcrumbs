using System;
using System.Diagnostics;
using Breadcrumbs.Events;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Breadcrumbs
{
    public class CameraItemInteracter : MonoBehaviour
    {
        [SerializeField] private float maxDistanceToInteract;
        [SerializeField] private Image grabIcon;
        [SerializeField] private Image interactIcon;
        [SerializeField] private ItemHolder item;
        [SerializeField] private InteractableHolder interactable;
        [SerializeField] private bool isDropping;
        private static CameraItemInteracter instance;

        public static CameraItemInteracter Instance => instance;
        public ItemHolder Item => item;
        public InteractableHolder Interactable => interactable;

        private void Awake()
        {
            instance = this;
        }


        //Gets called every frame to check if the player is looking on something interactive
        private void Update()
        {
            if (!isDropping)
            {
                RaycastHit _hit = new RaycastHit();
                if (Physics.Raycast(transform.position, transform.forward, out _hit, maxDistanceToInteract))
                {
                    if (interactable != null)
                    {
                        interactable.IsLookedAt = false;
                        interactable = null;
                    }

                    if (_hit.transform.tag == GameTags.ITEM)
                    {
                        if (item != _hit.transform)
                        {
                            interactIcon.gameObject.SetActive(false);
                            if (_hit.transform.GetComponent<ItemHolder>() != null)
                            {
                                item = _hit.transform.GetComponent<ItemHolder>();
                                item.IsLookedAt = true;
                                grabIcon.gameObject.SetActive(true);
                            }
                        }
                    }
                    else if (_hit.transform.tag == GameTags.INTERACTABLE)
                    {
                        if (item != null)
                        {
                            item.IsLookedAt = false;
                            item = null;
                        }

                        grabIcon.gameObject.SetActive(false);
                        if (_hit.transform.GetComponent<InteractableHolder>() != null)
                        {
                            interactable = _hit.transform.GetComponent<InteractableHolder>();
                            if (interactable.CanBeActivatedMultipleTimes || !interactable.WasActivated)
                            {
                                if (interactable.IsAvailable)
                                {
                                    if (GameManager.IsPlayingHansel)
                                    {
                                        if (interactable.CharacterThatShouldUseThis == ActingCharacters.HANSEL ||
                                            interactable.CharacterThatShouldUseThis == ActingCharacters.BOTH)
                                        {
                                            interactable.IsLookedAt = true;
                                            interactIcon.gameObject.SetActive(true);
                                        }
                                    }
                                    else
                                    {
                                        if (interactable.CharacterThatShouldUseThis == ActingCharacters.GRETEL ||
                                            interactable.CharacterThatShouldUseThis == ActingCharacters.BOTH)
                                        {
                                            interactable.IsLookedAt = true;
                                            interactIcon.gameObject.SetActive(true);
                                        }
                                    }
                                }
                                else
                                {
                                    if (interactable.ShouldBeVisibleIfNotAvailable)
                                    {
                                        if (GameManager.IsPlayingHansel)
                                        {
                                            if (interactable.CharacterThatShouldUseThis == ActingCharacters.HANSEL ||
                                                interactable.CharacterThatShouldUseThis == ActingCharacters.BOTH)
                                            {
                                                interactable.IsLookedAt = true;
                                                interactIcon.gameObject.SetActive(true);
                                            }
                                        }
                                        else
                                        {
                                            if (interactable.CharacterThatShouldUseThis == ActingCharacters.GRETEL ||
                                                interactable.CharacterThatShouldUseThis == ActingCharacters.BOTH)
                                            {
                                                interactable.IsLookedAt = true;
                                                interactIcon.gameObject.SetActive(true);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                grabIcon.gameObject.SetActive(false);
                                interactIcon.gameObject.SetActive(false);
                                if (item != null)
                                {
                                    item.IsLookedAt = false;
                                    item = null;
                                }

                                if (interactable != null)
                                {
                                    interactable.IsLookedAt = false;
                                    interactable = null;
                                }
                            }
                        }
                    }
                    else
                    {
                        grabIcon.gameObject.SetActive(false);
                        interactIcon.gameObject.SetActive(false);
                        if (item != null)
                        {
                            item.IsLookedAt = false;
                            item = null;
                        }

                        if (interactable != null)
                        {
                            interactable.IsLookedAt = false;
                            interactable = null;
                        }
                    }
                }
                else
                {
                    grabIcon.gameObject.SetActive(false);
                    interactIcon.gameObject.SetActive(false);
                    if (item != null)
                    {
                        item.IsLookedAt = false;
                        item = null;
                    }

                    if (interactable != null)
                    {
                        interactable.IsLookedAt = false;
                        interactable = null;
                    }
                }
            }
            else
            {
                if (item != null)
                {
                    item.IsLookedAt = false;
                    item = null;
                }

                if (interactable != null)
                {
                    interactable.IsLookedAt = false;
                    interactable = null;
                }
            }
        }

        public void OnDropReady(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                isDropping = true;
            }
            else
            {
                isDropping = false;
            }
        }

        //Gets called when left mouse button was clicked while looking on an item or interactable
        public void OnPickupLeft(InputAction.CallbackContext ctx)
        {
            if (ctx.started)
            {
                if (!isDropping)
                {
                    if (item != null)
                    {
                        OnPickup(PlayerHands.LEFT);
                    }
                    else if (interactable != null)
                    {
                        OnInteract(PlayerHands.LEFT);
                    }
                } else
                {
                    if (interactable == null)
                    {
                        OnDrop(PlayerHands.LEFT);
                    }
                }
            }
        }

        //Gets called when right mouse was clicked while looking on an item or interactable
        public void OnPickupRight(InputAction.CallbackContext ctx)
        {
            if (ctx.started)
            {
                if (!isDropping)
                {
                    if (item != null)
                    {
                        OnPickup(PlayerHands.RIGHT);
                    }
                    else if (interactable != null)
                    {
                        OnInteract(PlayerHands.RIGHT);
                    }
                } else
                {
                    if (interactable == null)
                    {
                        OnDrop(PlayerHands.RIGHT);
                    }
                }
            }
        }
        
        //There are three different actions a player could be doing when clicking and trying to hit something.
        
        //First they could pickup an item
        private void OnPickup(PlayerHands hand)
        {
            ActingCharacters activeCharacter = GameManager.IsPlayingHansel ? ActingCharacters.HANSEL : ActingCharacters.GRETEL;
            Message.Raise(new ItemPickupEvent(item.ScriptableItem, hand, activeCharacter));
            item = null;
        }

        //If they dont look at an item or an interactable they could drop something
        private void OnDrop(PlayerHands hand)
        {
            ActingCharacters activeCharacter = GameManager.IsPlayingHansel ? ActingCharacters.HANSEL : ActingCharacters.GRETEL;
            Message.Raise(new ItemDropEvent(null, hand, activeCharacter));
        }

        //If they are looking at an interactable, they could be trying to use an interactable
        private void OnInteract(PlayerHands hand)
        {
            if (interactable.NumberOfItemsNeeded > 0)
            {
                ActingCharacters activeCharacter = GameManager.IsPlayingHansel ? ActingCharacters.HANSEL : ActingCharacters.GRETEL;
                Message.Raise(new ItemUseEvent(interactable.NeededItem, hand, activeCharacter));
            }
            else
            {
                Message.Raise(new InteractEvent(interactable));
                interactable = null;
            }
        }
    }
}