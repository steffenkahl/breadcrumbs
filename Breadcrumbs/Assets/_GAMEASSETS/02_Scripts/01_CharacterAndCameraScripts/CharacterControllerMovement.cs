using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Breadcrumbs
{
    public class CharacterControllerMovement : MonoBehaviour
    {
        #region Serialized Variables---

        // Serializing values to assign them in the Unity Inspector
        [SerializeField] private float speed; //the speed the character uses when walking casually
        [SerializeField] private float runspeed; //the speed the character uses while running
        private float currentspeed; //the speed the character currently has
        private bool isSprinting; //to check weather or not Shift is pressed
        [SerializeField] private bool canMove;
        

        //REFERENCES
        [SerializeField] private Camera cameraOne;

        #endregion

        #region Other Variables---

        private CharacterController characterController; //Storing the CharacterController Component
        private Vector3 motion; //Vector3 we use to move the CharacterController


        private Vector2 rawInputAxis; //the input Movement from the Input System (declared in "OnMovement()")

        #endregion

        public bool CanMove
        {
            get => canMove;
            set => canMove = value;
        }

        /// <summary>
        /// Before we can go through with Update, we need to attach the script to a GameObject with a 
        /// CharacterController-Component to be able to even access a CharacterController and store it in a variable
        /// </summary>
        private void Awake()
        {
            characterController = GetComponent<CharacterController>();
            currentspeed = speed;
        }

        private void OnEnable()
        {
            Message<PlayerTeleportEvent>.Add(OnPlayerTeleportEvent);
        }

        private void OnDisable()
        {
            Message<PlayerTeleportEvent>.Remove(OnPlayerTeleportEvent);
        }

        private void Start()
        {
            cameraOne = Camera.main; //Get a reference to the main Camera
        }

        //Everything Physics Based should be in FixedUpdate to be consistent
        private void FixedUpdate()
        {
            //Get Input Axis:
            float horizontalInput = rawInputAxis.x;
            float verticalInput = rawInputAxis.y;

            //Here we get the direction of the camera to make the movement relative to that
            Vector3 cameraForward = cameraOne.transform.forward;
            Vector3 cameraRight = cameraOne.transform.right;
            cameraForward.y = 0f;
            cameraRight.y = 0f;
            cameraForward.Normalize();
            cameraRight.Normalize();

            
            // We need to get the input values of the axis every frame in order tu move in the direction of our input
            // x is the horizontalInput value which will move the characterController right or left
            // y is set to the gravity value which will pull the characterController down 
            // z is set to the verticalInput value which will move the characterController forwards or backwards
            //motion = new Vector3(horizontalInput, gravity, verticalInput);
            Vector3 desiredMoveDirection = (cameraForward * verticalInput + cameraRight * horizontalInput) * currentspeed;

            if (canMove)
            {
                motion = desiredMoveDirection - transform.up;
            }
            else
            {
                motion = Vector3.zero;
            }





            // We apply the Vector for movement (named "motion") to the .Move() method of the characterController 
            // In order to stay framerate independent we multiply the vector by Time.deltaTime and also
            // by the moveSpeed in order to maintain control of speed
            characterController.Move(motion: motion * Time.deltaTime);

            //Calculate current Speed:
            if (isSprinting == false) 
            {
                currentspeed = speed;
            }
            else
            {
               currentspeed = runspeed;
            }

        }

        //The input Actions that get called from the new input manager

        #region InputActions

        //Gets called when the Player uses Movement-Keys/Buttons
        public void OnMovement(InputAction.CallbackContext value)
        {
            if (PlatformHelper.ActiveDeviceType != DeviceType.VR)
            {
                rawInputAxis = value.ReadValue<Vector2>();
            }
        }

        public void OnInteract(InputAction.CallbackContext value)
        {
            if (value.started)
            {
                Debug.Log(message: "Player interacted with an object");
            }
        }
        public void OnSprint(InputAction.CallbackContext value)
        {
            if (value.performed)
            {
                    isSprinting = true;
            }
            else
            {
                    isSprinting = false;
            }
            
        }
        #endregion

        public void OnGameQuit()
        {
            Application.Quit();
            Debug.Log("Game quitting!");
        }

        public void OnCharacterSwitch(InputAction.CallbackContext value)
        {
            if (value.started)
            {
                Message.Raise(new CharacterSwitchEvent());
            }
        }

        public void OnPlayerTeleportEvent(PlayerTeleportEvent ctx)
        {
            characterController.enabled = false;
            transform.position = ctx.NewPosition;
            characterController.enabled = true;
        }
    }
}