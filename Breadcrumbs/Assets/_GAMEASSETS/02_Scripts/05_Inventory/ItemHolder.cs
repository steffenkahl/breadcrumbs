﻿using Breadcrumbs.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Breadcrumbs
{
    public class ItemHolder : MonoBehaviour
    {
        [SerializeField] private Item scriptableItem;
        [SerializeField] private bool isLookedAt;
        public UnityEvent OnPickup;
        public Item ScriptableItem => scriptableItem;

        public bool IsLookedAt
        {
            get => isLookedAt;
            set => isLookedAt = value;
        }
        
        private void OnEnable()
        {
            Message<ItemPickupSuccessEvent>.Add(OnItemPickupSuccess);
        }

        private void OnDisable()
        {
            Message<ItemPickupSuccessEvent>.Remove(OnItemPickupSuccess);
        }

        private void OnItemPickupSuccess(ItemPickupSuccessEvent ctx)
        {
            if (CameraItemInteracter.Instance.Item == this)
            {
                if (ctx.PickedUpItem == scriptableItem && isLookedAt)
                {
                    OnPickup.Invoke();
                    gameObject.SetActive(false);
                }
            }
        }
    }
}