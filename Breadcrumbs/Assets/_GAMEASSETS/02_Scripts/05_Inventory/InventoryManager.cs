﻿using System.Collections.Generic;
using Breadcrumbs.Events;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Breadcrumbs
{
    public class InventoryManager : MonoBehaviour
    {
        [SerializeField] private Sprite emptyItemSprite;
        
        [Header("References")]
        [SerializeField] private Image leftHandItemImage;
        [SerializeField] private TMP_Text leftHandItemAmountTXT;
        [SerializeField] private TMP_Text leftHandItemNameTXT;
        [SerializeField] private Image rightHandItemImage;
        [SerializeField] private TMP_Text rightHandItemAmountTXT;
        [SerializeField] private TMP_Text rightHandItemNameTXT;

        //Items and Amounts:
        private Dictionary<ActingCharacters, Dictionary<PlayerHands, Item>> handItem; //A list of itemTypes in hand
        private Dictionary<ActingCharacters, Dictionary<PlayerHands, int>> handItemAmount; //The amounts of items in hand

        //Setup Events:
        private void OnEnable()
        {
            Message<ItemPickupEvent>.Add(OnItemPickupEvent);
            Message<ItemUseEvent>.Add(OnItemUseEvent);
            Message<InventoryClearEvent>.Add(OnInventoryClearEvent);
            Message<CharacterSwitchSuccessEvent>.Add(OnCharacterSwitchSuccessEvent);
            Message<ItemDropEvent>.Add(OnItemDropEvent);
        }
        private void OnDisable()
        {
            Message<ItemPickupEvent>.Remove(OnItemPickupEvent);
            Message<ItemUseEvent>.Remove(OnItemUseEvent);
            Message<InventoryClearEvent>.Remove(OnInventoryClearEvent);
            Message<CharacterSwitchSuccessEvent>.Remove(OnCharacterSwitchSuccessEvent);
            Message<ItemDropEvent>.Remove(OnItemDropEvent);
        }

        private void Start()
        {
            //Create Dictionaries:
            handItem = new Dictionary<ActingCharacters, Dictionary<PlayerHands, Item>>();
            handItem[ActingCharacters.GRETEL] = new Dictionary<PlayerHands, Item>();
            handItem[ActingCharacters.HANSEL] = new Dictionary<PlayerHands, Item>();
            handItem[ActingCharacters.GRETEL].Add(PlayerHands.LEFT, null);
            handItem[ActingCharacters.GRETEL].Add(PlayerHands.RIGHT, null);
            handItem[ActingCharacters.HANSEL].Add(PlayerHands.LEFT, null);
            handItem[ActingCharacters.HANSEL].Add(PlayerHands.RIGHT, null);

            handItemAmount = new Dictionary<ActingCharacters, Dictionary<PlayerHands, int>>();
            handItemAmount[ActingCharacters.GRETEL] = new Dictionary<PlayerHands, int>();
            handItemAmount[ActingCharacters.HANSEL] = new Dictionary<PlayerHands, int>();
            handItemAmount[ActingCharacters.GRETEL].Add(PlayerHands.LEFT, 0);
            handItemAmount[ActingCharacters.GRETEL].Add(PlayerHands.RIGHT, 0);
            handItemAmount[ActingCharacters.HANSEL].Add(PlayerHands.LEFT, 0);
            handItemAmount[ActingCharacters.HANSEL].Add(PlayerHands.RIGHT, 0);
            
            UpdateItemAmounts();
        }

        /// <summary>
        /// Picks up Item when "ItemPickupEvent" gets called
        /// </summary>
        /// <param name="ctx">ItemPickupEvent</param>
        private void OnItemPickupEvent(ItemPickupEvent ctx)
        {
            if (handItem[ctx.ActingCharacter][ctx.UsedHand] == null)
            {
                handItem[ctx.ActingCharacter][ctx.UsedHand] = ctx.PickedUpItem;
                leftHandItemImage.sprite = ctx.PickedUpItem.ItemIcon;
                handItemAmount[ctx.ActingCharacter][ctx.UsedHand] = 1;
                Message.Raise(new ItemPickupSuccessEvent(ctx.PickedUpItem, ctx.UsedHand, ctx.ActingCharacter));
            }
            else if (ctx.PickedUpItem.ItemName == handItem[ctx.ActingCharacter][ctx.UsedHand].ItemName)
            {
                handItemAmount[ctx.ActingCharacter][ctx.UsedHand] += 1;
                Message.Raise(new ItemPickupSuccessEvent(ctx.PickedUpItem, ctx.UsedHand, ctx.ActingCharacter));
            }
            else
            {
                Message.Raise(new ItemPickupFailEvent(ctx.PickedUpItem, ctx.UsedHand, ctx.ActingCharacter));
            }
            
            UpdateItemAmounts();
        }
        
        /// <summary>
        /// Uses Item when "ItemUseEvent" gets called
        /// </summary>
        /// <param name="ctx">ItemUseEvent</param>
        private void OnItemUseEvent(ItemUseEvent ctx)
        {
            if (handItem[ctx.ActingCharacter][ctx.UsedHand] == null)
            {
                Message.Raise(new ItemUseFailEvent(ctx.UsedItem, ctx.UsedHand, ctx.ActingCharacter));
            }
            else if (ctx.UsedItem.ItemName == handItem[ctx.ActingCharacter][ctx.UsedHand].ItemName)
            {
                if (handItemAmount[ctx.ActingCharacter][ctx.UsedHand] > 1)
                {
                    handItemAmount[ctx.ActingCharacter][ctx.UsedHand] -= 1;
                    Message.Raise(new ItemUseSuccessEvent(ctx.UsedItem, ctx.UsedHand, ctx.ActingCharacter));
                }
                else
                {
                    handItem[ctx.ActingCharacter][ctx.UsedHand] = null;
                    handItemAmount[ctx.ActingCharacter][ctx.UsedHand] = 0;
                    Message.Raise(new ItemUseSuccessEvent(ctx.UsedItem, ctx.UsedHand, ctx.ActingCharacter));
                }
            }
            
            UpdateItemAmounts();
        }
        
        /// <summary>
        /// Drops Item when "ItemDropEvent" gets called
        /// </summary>
        /// <param name="ctx">ItemDropEvent</param>
        private void OnItemDropEvent(ItemDropEvent ctx)
        {
            Item droppedItem;
            if (ctx.DroppedItem != null)
            {
                droppedItem = ctx.DroppedItem;
            }
            else
            {
                if (handItem[ctx.ActingCharacter][ctx.UsedHand] != null)
                {
                    droppedItem = handItem[ctx.ActingCharacter][ctx.UsedHand];
                }
                else
                {
                    Message.Raise(new ItemUseFailEvent(null, ctx.UsedHand, ctx.ActingCharacter, "No Item to use in Inventory"));
                    return;
                }
            }

            if (droppedItem.IsDroppable)
            {
                if (handItem[ctx.ActingCharacter][ctx.UsedHand] == null)
                {
                    Message.Raise(new ItemUseFailEvent(droppedItem, ctx.UsedHand, ctx.ActingCharacter));
                }
                else if (droppedItem.ItemName == handItem[ctx.ActingCharacter][ctx.UsedHand].ItemName)
                {
                    if (handItemAmount[ctx.ActingCharacter][ctx.UsedHand] > 1)
                    {
                        handItemAmount[ctx.ActingCharacter][ctx.UsedHand] -= 1;
                        Message.Raise(new ItemDropSuccessEvent(droppedItem, ctx.UsedHand, ctx.ActingCharacter));
                    }
                    else
                    {
                        handItem[ctx.ActingCharacter][ctx.UsedHand] = null;
                        leftHandItemImage.sprite = emptyItemSprite;
                        handItemAmount[ctx.ActingCharacter][ctx.UsedHand] = 0;
                        Message.Raise(new ItemDropSuccessEvent(droppedItem, ctx.UsedHand, ctx.ActingCharacter));
                    }
                }
                else
                {
                    Message.Raise(new ItemUseFailEvent(droppedItem, ctx.UsedHand, ctx.ActingCharacter));
                }
            }
            else
            {
                Message.Raise(new ItemUseFailEvent(droppedItem, ctx.UsedHand, ctx.ActingCharacter, "This Item can't be dropped!"));
            }

            UpdateItemAmounts();
        }

        /// <summary>
        /// Clear Inventory when "InventoryClearEvent" gets called
        /// </summary>
        /// <param name="ctx">InventoryClearEvent</param>
        private void OnInventoryClearEvent(InventoryClearEvent ctx)
        {
            handItem.Clear();
            handItemAmount.Clear();
            
            UpdateItemAmounts();
        }

        /// <summary>
        /// Update the displayed Values for Item Amounts
        /// </summary>
        private void UpdateItemAmounts()
        {
            ActingCharacters activeCharacter;
            if (GameManager.IsPlayingHansel)
            {
                activeCharacter = ActingCharacters.HANSEL;
            }
            else
            {
                activeCharacter = ActingCharacters.GRETEL;
            }

            if (handItemAmount[activeCharacter][PlayerHands.LEFT] > 0)
            {
                leftHandItemAmountTXT.text = handItemAmount[activeCharacter][PlayerHands.LEFT].ToString();
                leftHandItemNameTXT.text = handItem[activeCharacter][PlayerHands.LEFT].ItemName;
                leftHandItemImage.sprite = handItem[activeCharacter][PlayerHands.LEFT].ItemIcon;
            }
            else
            {
                leftHandItemAmountTXT.text = "";
                leftHandItemNameTXT.text = "";
                leftHandItemImage.sprite = emptyItemSprite;
            }
            
            if (handItemAmount[activeCharacter][PlayerHands.RIGHT] > 0)
            {
                rightHandItemAmountTXT.text = handItemAmount[activeCharacter][PlayerHands.RIGHT].ToString();
                rightHandItemNameTXT.text = handItem[activeCharacter][PlayerHands.RIGHT].ItemName;
                rightHandItemImage.sprite = handItem[activeCharacter][PlayerHands.RIGHT].ItemIcon;
            }
            else
            {
                rightHandItemAmountTXT.text = "";
                rightHandItemNameTXT.text = "";
                rightHandItemImage.sprite = emptyItemSprite;
            }
        }

        private void OnCharacterSwitchSuccessEvent(CharacterSwitchSuccessEvent ctx)
        {
            UpdateItemAmounts();
        }
    }
}