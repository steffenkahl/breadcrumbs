using Breadcrumbs.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Breadcrumbs
{
    public class InteractableHolder : MonoBehaviour
    {
        [SerializeField] private bool shouldBeVisibleIfNotAvailable = true;
        [SerializeField] private ActingCharacters characterThatShouldUseThis = ActingCharacters.BOTH;
        [SerializeField] private bool isAvailable;
        [SerializeField] private TextFeedback textFeedbackIfNotAvailable;
        [SerializeField] private Item neededItem;
        [SerializeField] private int numberOfItemsNeeded = 1;
        [SerializeField] private bool isLookedAt;
        [SerializeField] private bool canBeActivatedMultipleTimes;
        public UnityEvent OnActivation;

        private bool wasActivated;

        public bool IsLookedAt
        {
            get => isLookedAt;
            set => isLookedAt = value;
        }
        public bool WasActivated
        {
            get => wasActivated;
            set => wasActivated = value;
        }
        public bool IsAvailable
        {
            get => isAvailable;
            set => isAvailable = value;
        }
        public bool ShouldBeVisibleIfNotAvailable
        {
            get => shouldBeVisibleIfNotAvailable;
            set => shouldBeVisibleIfNotAvailable = value;
        }

        public ActingCharacters CharacterThatShouldUseThis
        {
            get => characterThatShouldUseThis;
            set => characterThatShouldUseThis = value;
        }

        public int NumberOfItemsNeeded => numberOfItemsNeeded;
        public bool CanBeActivatedMultipleTimes => canBeActivatedMultipleTimes;

        public Item NeededItem => neededItem;

        private void OnEnable()
        {
            Message<ItemUseSuccessEvent>.Add(OnItemUseSuccessEvent);
            Message<InteractEvent>.Add(OnInteractEvent);
        }

        private void OnDisable()
        {
            Message<ItemUseSuccessEvent>.Remove(OnItemUseSuccessEvent);
            Message<InteractEvent>.Remove(OnInteractEvent);
        }

        private void OnItemUseSuccessEvent(ItemUseSuccessEvent ctx)
        {
            if (isAvailable)
            {
                if (ctx.UsedItem == neededItem && isLookedAt)
                {
                    if (numberOfItemsNeeded <= 1)
                    {
                        OnActivation.Invoke();
                        wasActivated = true;
                    }
                    else
                    {
                        numberOfItemsNeeded -= 1;
                    }
                }
            }
        }

        private void OnInteractEvent(InteractEvent ctx)
        {
            if (ctx.InteractableHolder == this)
            {
                if (GameManager.IsPlayingHansel == (characterThatShouldUseThis == ActingCharacters.HANSEL) || characterThatShouldUseThis == ActingCharacters.BOTH)
                {
                    if (isAvailable)
                    {
                        if (numberOfItemsNeeded < 1)
                        {
                            OnActivation.Invoke();
                            wasActivated = true;
                        }
                        else
                        {
                            if (textFeedbackIfNotAvailable)
                            {
                                Debug.Log("Not Completed!");
                                Message.Raise(new ShowTextFeedbackEvent(textFeedbackIfNotAvailable));
                            }
                        }
                    }
                    else
                    {
                        if (textFeedbackIfNotAvailable)
                        {
                            Debug.Log("Not Available!");
                            Message.Raise(new ShowTextFeedbackEvent(textFeedbackIfNotAvailable));
                        }
                    }
                }
            }
        }

        public void ActivateHolder()
        {
            OnActivation.Invoke();
        }
    }
}