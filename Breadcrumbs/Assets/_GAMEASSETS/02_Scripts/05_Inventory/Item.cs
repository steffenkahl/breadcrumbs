﻿using UnityEngine;

namespace Breadcrumbs
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Breadcrumbs/Item", order = 51)]
    public class Item : ScriptableObject
    {
        [SerializeField] private GameObject[] itemPrefab;
        [SerializeField] private Sprite itemIcon;
        [SerializeField] private string itemName;
        [SerializeField] private string itemDescription;
        [SerializeField] private bool isDroppable;

        public GameObject[] ItemPrefab => itemPrefab;
        public Sprite ItemIcon => itemIcon;
        public string ItemName => itemName;
        public string ItemDescription => itemDescription;
        public bool IsDroppable => isDroppable;
    }
}