﻿using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class DropManager : MonoBehaviour
    {
        [SerializeField] private float dropDistance;

        private void OnEnable()
        {
            Message<ItemDropSuccessEvent>.Add(OnItemDropSuccessEvent);
        }
        
        private void OnDisable()
        {
            Message<ItemDropSuccessEvent>.Remove(OnItemDropSuccessEvent);
        }

        private void OnItemDropSuccessEvent(ItemDropSuccessEvent ctx)
        {
            GameObject spawnedPrefab = ctx.DroppedItem.ItemPrefab[Random.Range(0, ctx.DroppedItem.ItemPrefab.Length)];
            Instantiate(spawnedPrefab, transform.position + transform.forward * dropDistance, Quaternion.Euler(0,0,0));
        }
    }
}