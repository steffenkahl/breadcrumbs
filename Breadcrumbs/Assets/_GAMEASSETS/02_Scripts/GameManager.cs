using System;
using Breadcrumbs.Events;
using UnityEngine;

namespace Breadcrumbs
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager instance;
        //Hansel is character 1 and this static variable returns if the player is playing him
        private static bool isPlayingHansel;
        [SerializeField] private bool canPlayHansel;
        [SerializeField] private bool canPlayGretel;

        public static bool IsPlayingHansel => isPlayingHansel;

        public bool CanPlayGretel
        {
            get => canPlayGretel;
            set => canPlayGretel = value;
        }

        public bool CanPlayHansel
        {
            get => canPlayHansel;
            set => canPlayHansel = value;
        }

        public static GameManager Instance => instance;

        private void OnEnable()
        {
            Message<CharacterSwitchEvent>.Add(OnCharacterSwitchEvent);
        }

        private void OnDisable()
        {
            Message<CharacterSwitchEvent>.Remove(OnCharacterSwitchEvent);
        }

        private void Awake()
        {
            instance = this;
        }

        public void OnCharacterSwitchEvent(CharacterSwitchEvent ctx)
        {
            if (isPlayingHansel && canPlayGretel)
            {
                isPlayingHansel = false;
                Message.Raise(new CharacterSwitchSuccessEvent(isPlayingHansel));
            }
            else if (!isPlayingHansel && canPlayHansel)
            {
                isPlayingHansel = true;
                Message.Raise(new CharacterSwitchSuccessEvent(isPlayingHansel));
            }
            else
            {
                Message.Raise(new CharacterSwitchFailEvent(isPlayingHansel));
            }
        }
    }
}