﻿namespace Breadcrumbs
{
    public static class GameTags
    {
        public static string PLAYER = "Player";
        public static string ITEM = "Item";
        public static string INTERACTABLE = "Interactable";
        public static string SIBLING = "Sibling";
        public static string SETTINGS = "Settings";
    }
}