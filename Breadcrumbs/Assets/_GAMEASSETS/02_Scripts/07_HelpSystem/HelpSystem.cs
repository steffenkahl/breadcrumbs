using System;
using Breadcrumbs.Events;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Breadcrumbs
{
    public class HelpSystem : MonoBehaviour
    {
        [SerializeField] private Animator tipAnim;
        [SerializeField] private TMP_Text TipTitle;
        [SerializeField] private TMP_Text TipContent;
        [SerializeField] private bool tipFieldIsOpen;

        private void OnEnable()
        {
            Message<DisplayTutorialEvent>.Add(OnDisplayTutorialEvent);
            Message<ContinueDialogueEvent>.Add(OnContinueDialogueEvent);
            Message<ChangeHelpTextEvent>.Add(OnChangeHelpTextEvent);
        }
        
        private void OnDisable()
        {
            Message<DisplayTutorialEvent>.Remove(OnDisplayTutorialEvent);
            Message<ContinueDialogueEvent>.Remove(OnContinueDialogueEvent);
            Message<ChangeHelpTextEvent>.Add(OnChangeHelpTextEvent);
        }

        private void OnDisplayTutorialEvent(DisplayTutorialEvent ctx)
        {
            tipFieldIsOpen = true;
            TipTitle.text = ctx.Text.TipTitle;
            TipContent.text = ctx.Text.TipContent;
            OpenCloseTipField(tipFieldIsOpen);
        }

        private void OnContinueDialogueEvent(ContinueDialogueEvent ctx)
        {
            OpenCloseTipField(false);
        }

        private void OnChangeHelpTextEvent(ChangeHelpTextEvent ctx)
        {
            TipTitle.text = ctx.Text.TipTitle;
            TipContent.text = ctx.Text.TipContent;
            OpenCloseTipField(false);
        }

        public void OpenCloseTips(InputAction.CallbackContext value)
        {
            if (value.started)
            {
                OpenCloseTipFieldButton(!tipFieldIsOpen);
            }
        }

        private void OpenCloseTipField(bool open)
        {
            if (open)
            {
                tipFieldIsOpen = open; //Shows/Hides the tipField
            }
            
            tipAnim.SetBool("open", tipFieldIsOpen);
        }
        
        private void OpenCloseTipFieldButton(bool open)
        {
            tipFieldIsOpen = open; //Shows/Hides the tipField
            
            tipAnim.SetBool("open", tipFieldIsOpen);
        }
    }
}