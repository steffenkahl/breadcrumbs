﻿using UnityEngine;

namespace Breadcrumbs
{
    [CreateAssetMenu(fileName = "NewHelpText", menuName = "Breadcrumbs/HelpText", order = 1)]
    public class HelpText : ScriptableObject
    {
        [SerializeField] private string tipTitle;

        [SerializeField] private bool tutorial;
        
        [SerializeField] [TextArea(3,10)]
        private string tipContent; //Adding the displayed sentence the speaker says in the Unity Inspector

        public string TipTitle => tipTitle;
        public string TipContent => tipContent;
        public bool Tutorial => tutorial;
    }
}