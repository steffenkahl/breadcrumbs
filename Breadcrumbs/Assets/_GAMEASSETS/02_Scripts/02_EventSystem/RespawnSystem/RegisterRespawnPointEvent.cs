﻿namespace Breadcrumbs.Events
{
    public class RegisterRespawnPointEvent
    {
        private RespawnPoint newPoint;

        public RespawnPoint NewPoint => newPoint;

        public RegisterRespawnPointEvent(RespawnPoint newPoint)
        {
            this.newPoint = newPoint;
        }
    }
}