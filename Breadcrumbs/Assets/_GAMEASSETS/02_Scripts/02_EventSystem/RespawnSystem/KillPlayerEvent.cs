﻿using UnityEngine;

namespace Breadcrumbs.Events
{
    public class KillPlayerEvent
    {
        private Vector3 deathPoint;

        public Vector3 DeathPoint => deathPoint;
        
        public KillPlayerEvent(Vector3 deathPoint)
        {
            this.deathPoint = deathPoint;
        }
    }
}