﻿using UnityEngine;

namespace Breadcrumbs.Events
{
    public class PlayerRespawnedEvent
    {
        private bool keepsSibling;
        private Vector3 respawnPosition;

        public bool KeepsSibling => keepsSibling;
        public Vector3 RespawnPosition => respawnPosition;

        public PlayerRespawnedEvent(bool keepsSibling, Vector3 respawnPosition)
        {
            this.keepsSibling = keepsSibling;
            this.respawnPosition = respawnPosition;
        }
    }
}