﻿namespace Breadcrumbs.Events
{
    public class ChangeHelpTextEvent
    {
        private HelpText text;

        public HelpText Text => text;

        public ChangeHelpTextEvent(HelpText text)
        {
            this.text = text;
        }
    }
}