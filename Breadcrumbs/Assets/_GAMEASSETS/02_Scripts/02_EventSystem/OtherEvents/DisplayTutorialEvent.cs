﻿namespace Breadcrumbs.Events
{
    public class DisplayTutorialEvent
    {
        private HelpText text;

        public HelpText Text => text;

        public DisplayTutorialEvent(HelpText text)
        {
            this.text = text;
        }
    }
}