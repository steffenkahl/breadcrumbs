﻿namespace Breadcrumbs.Events.OtherEvents
{
    public class ChangeTimeEvent
    {
        private float newTime;

        public float NewTime => newTime;

        public ChangeTimeEvent(float newTime)
        {
            this.newTime = newTime;
        }
    }
}