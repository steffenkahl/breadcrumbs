﻿using UnityEngine;

namespace Breadcrumbs.Events.OtherEvents
{
    public class PlayerTeleportEvent
    {
        private Vector3 newPosition;

        public Vector3 NewPosition => newPosition;

        public PlayerTeleportEvent(Vector3 newPosition)
        {
            this.newPosition = newPosition;
        }
    }
}