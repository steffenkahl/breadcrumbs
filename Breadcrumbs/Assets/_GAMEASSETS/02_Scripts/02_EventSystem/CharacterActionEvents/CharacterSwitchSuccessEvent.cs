﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Gets called if a CharacterSwitchEvent was successful
    /// </summary>
    public class CharacterSwitchSuccessEvent
    {
        private bool isPlayingHansel;
        public bool IsPlayingHansel => isPlayingHansel;

        public CharacterSwitchSuccessEvent (bool isPlayingHansel)
        {
            this.isPlayingHansel = isPlayingHansel;
        }
    }
}