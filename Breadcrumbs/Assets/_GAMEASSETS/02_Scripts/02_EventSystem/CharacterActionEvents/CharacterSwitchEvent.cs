﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event to try to switch Characters. Gets ended with CharacterSwitchFailEvent or CharacterSwitchSuccessEvent
    /// </summary>
    public class CharacterSwitchEvent
    {
    }
}