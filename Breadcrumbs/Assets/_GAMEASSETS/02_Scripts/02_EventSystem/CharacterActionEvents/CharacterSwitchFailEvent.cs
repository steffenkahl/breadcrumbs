﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Gets called if a CharacterSwitchEvent failed
    /// </summary>
    public class CharacterSwitchFailEvent
    {
        private bool isPlayingHansel;

        public bool IsPlayingHansel => isPlayingHansel;

        public CharacterSwitchFailEvent (bool isPlayingHansel)
        {
            this.isPlayingHansel = isPlayingHansel;
        }
    }
}