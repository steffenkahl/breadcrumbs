﻿namespace Breadcrumbs.Events
{
    public class ShowTextFeedbackEvent
    {
        private TextFeedback feedback;

        public TextFeedback Feedback => feedback;

        public ShowTextFeedbackEvent(TextFeedback feedback)
        {
            this.feedback = feedback;
        }
    }
}