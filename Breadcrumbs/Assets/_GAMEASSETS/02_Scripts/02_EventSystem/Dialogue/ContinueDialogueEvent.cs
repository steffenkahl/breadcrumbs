﻿namespace Breadcrumbs.Events
{
    public class ContinueDialogueEvent
    {
        private int chapter;

        public int Chapter => chapter;

        public ContinueDialogueEvent(int chapter)
        {
            this.chapter = chapter;
        }
    }
}