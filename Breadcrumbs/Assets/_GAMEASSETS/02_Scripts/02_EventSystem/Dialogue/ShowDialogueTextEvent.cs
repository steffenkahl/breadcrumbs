﻿namespace Breadcrumbs.Events
{
    public class ShowDialogueTextEvent
    {
        private Dialogue shownDialogue;
        private DialogueTrigger usedTrigger;

        public Dialogue ShownDialogue => shownDialogue;
        public DialogueTrigger UsedTrigger => usedTrigger;

        public ShowDialogueTextEvent(Dialogue shownDialogue, DialogueTrigger usedTrigger)
        {
            this.shownDialogue = shownDialogue;
            this.usedTrigger = usedTrigger;
        }
    }
}