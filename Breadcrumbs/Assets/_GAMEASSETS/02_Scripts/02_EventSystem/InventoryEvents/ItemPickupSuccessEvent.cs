﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event that gets called if the ItemPickupEvent was successful
    /// </summary>
    public class ItemPickupSuccessEvent
    {
        private Item pickedUpItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item PickedUpItem => pickedUpItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemPickupSuccessEvent(Item pickedUpItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.pickedUpItem = pickedUpItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}