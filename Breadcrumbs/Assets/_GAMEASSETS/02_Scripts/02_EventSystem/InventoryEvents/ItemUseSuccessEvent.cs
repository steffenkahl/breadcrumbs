﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event that gets called if the ItemUseEvent was successful
    /// </summary>
    public class ItemUseSuccessEvent
    {
        private Item usedItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item UsedItem => usedItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemUseSuccessEvent(Item usedItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.usedItem = usedItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}