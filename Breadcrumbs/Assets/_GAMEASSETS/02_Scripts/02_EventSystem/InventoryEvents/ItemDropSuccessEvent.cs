﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event that gets called if the ItemDropEvent was successful
    /// </summary>
    public class ItemDropSuccessEvent
    {
        private Item droppedItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item DroppedItem => droppedItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemDropSuccessEvent(Item droppedItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.droppedItem = droppedItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}