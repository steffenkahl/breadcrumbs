﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event for all interactions that dont use items
    /// </summary>
    public class InteractEvent
    {
        private InteractableHolder interactableHolder;

        public InteractableHolder InteractableHolder => interactableHolder;

        public InteractEvent(InteractableHolder interactableHolder)
        {
            this.interactableHolder = interactableHolder;
        }
    }
}