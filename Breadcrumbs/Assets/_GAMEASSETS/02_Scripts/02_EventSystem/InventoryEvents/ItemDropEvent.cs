﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event for trying to drop the currently holded item. Is ended with ItemDropFailEvent and ItemDropSuccessEvent
    /// </summary>
    public class ItemDropEvent
    {
        private Item droppedItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item DroppedItem => droppedItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemDropEvent(Item droppedItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.droppedItem = droppedItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}