﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event for trying to pickup an item. Is ended with ItemPickupFailEvent and ItemPickupSuccessEvent
    /// </summary>
    public class ItemPickupEvent
    {
        private Item pickedUpItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item PickedUpItem => pickedUpItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemPickupEvent(Item pickedUpItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.pickedUpItem = pickedUpItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}