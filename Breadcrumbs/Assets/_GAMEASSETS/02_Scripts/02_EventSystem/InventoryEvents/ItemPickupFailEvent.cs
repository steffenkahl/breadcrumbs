﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event that gets called if the ItemPickupEvent failed
    /// </summary>
    public class ItemPickupFailEvent
    {
        private Item pickedUpItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item PickedUpItem => pickedUpItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemPickupFailEvent(Item pickedUpItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.pickedUpItem = pickedUpItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}