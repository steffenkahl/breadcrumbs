﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event that gets called if the ItemUseEvent failed
    /// </summary>
    public class ItemUseFailEvent
    {
        private Item usedItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;
        private string reason;

        public Item UsedItem => usedItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        public string Reason => reason;
        
        public ItemUseFailEvent(Item usedItem, PlayerHands usedHand, ActingCharacters actingCharacter, string reason = "")
        {
            this.usedItem = usedItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
            this.reason = reason;
        }
    }
}