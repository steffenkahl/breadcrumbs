﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event that gets called if the ItemDropEvent failed
    /// </summary>
    public class ItemDropFailEvent
    {
        private Item droppedItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item DroppedItem => droppedItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemDropFailEvent(Item droppedItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.droppedItem = droppedItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}