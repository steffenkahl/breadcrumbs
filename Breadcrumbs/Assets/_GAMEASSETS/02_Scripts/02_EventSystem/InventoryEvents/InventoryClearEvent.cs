﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event for clearing the inventory
    /// </summary>
    public class InventoryClearEvent
    { }
}