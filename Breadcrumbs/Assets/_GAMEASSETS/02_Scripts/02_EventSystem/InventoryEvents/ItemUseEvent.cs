﻿namespace Breadcrumbs.Events
{
    /// <summary>
    /// Event for trying to use the currently holded item. Is ended with ItemUseFailEvent and ItemUseSuccessEvent
    /// </summary>
    public class ItemUseEvent
    {
        private Item usedItem;
        private PlayerHands usedHand;
        private ActingCharacters actingCharacter;

        public Item UsedItem => usedItem;
        public PlayerHands UsedHand => usedHand;
        public ActingCharacters ActingCharacter => actingCharacter;
        
        public ItemUseEvent(Item usedItem, PlayerHands usedHand, ActingCharacters actingCharacter)
        {
            this.usedItem = usedItem;
            this.usedHand = usedHand;
            this.actingCharacter = actingCharacter;
        }
    }
}