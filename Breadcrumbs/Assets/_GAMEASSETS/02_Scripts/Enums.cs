﻿namespace Breadcrumbs
{
    public class Enums
    { }
    
    /// <summary>
    /// Enum for all possible InventorySlots
    /// </summary>
    public enum PlayerHands
    {
        NONE,
        RIGHT,
        LEFT
    }
    
    /// <summary>
    /// Enum for all available playable Characters
    /// </summary>
    public enum ActingCharacters
    {
        NONE,
        HANSEL,
        GRETEL,
        BOTH,
    }

    /// <summary>
    /// Enum for all Characters that exist in Game for colors in dialogues and such
    /// </summary>
    public enum Characters
    {
        NONE,
        NARRATOR,
        HANSEL,
        GRETEL,
        FATHER,
        MOTHER,
        WITCH,
        TUTORIAL
    }
    
    public enum Areas
    {
        STANDARD,
        DEEPFOREST,
        DEATHZONE,
    }

    public enum AfterDialogueActions
    {
        NONE,
        NEXTDIALOGUE,
        NEXTCHAPTER,
        DAYNIGHTSWITCH,
        ACTION,
        ACTIONANDNEXTDIALOGUE
    }

    public enum SoundType
    {
        NONE,
        MAIN,
        MUSIC,
        VOICE,
        EFFECTS
    }
}