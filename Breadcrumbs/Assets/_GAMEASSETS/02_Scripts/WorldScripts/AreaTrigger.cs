﻿using System;
using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using UnityEngine;

namespace Breadcrumbs
{
    public class AreaTrigger : MonoBehaviour
    {
        [SerializeField] private Areas typeOfArea = Areas.DEATHZONE;

        private void Start()
        {
            GetComponent<Renderer>().enabled = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                switch (typeOfArea)
                {
                    case Areas.DEATHZONE:
                        Message.Raise(new KillPlayerEvent(other.transform.position));
                        break;
                    case Areas.DEEPFOREST:
                        Message.Raise(new EnterDeepForestEvent());
                        break;
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(GameTags.PLAYER))
            {
                switch (typeOfArea)
                {
                    case Areas.DEEPFOREST:
                        Message.Raise(new LeaveDeepForestEvent());
                        break;
                }
            }
        }
    }
}