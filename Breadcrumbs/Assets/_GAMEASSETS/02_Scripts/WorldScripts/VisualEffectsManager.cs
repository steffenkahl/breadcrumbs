﻿using System;
using Breadcrumbs.Events.OtherEvents;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Breadcrumbs.Events
{
    public class VisualEffectsManager : MonoBehaviour
    {
        [SerializeField] private float addedFogInDeepForest;
        private float standardFogDensity;
        
        private void OnEnable()
        {
            Message<EnterDeepForestEvent>.Add(OnEnterDeepForestEvent);
            Message<LeaveDeepForestEvent>.Add(OnLeaveDeepForestEvent);
        }

        private void OnDisable()
        {
            Message<EnterDeepForestEvent>.Remove(OnEnterDeepForestEvent);
            Message<LeaveDeepForestEvent>.Remove(OnLeaveDeepForestEvent);
        }

        private void OnEnterDeepForestEvent(EnterDeepForestEvent ctx)
        {
            RenderSettings.fogDensity = standardFogDensity + addedFogInDeepForest;
        }

        private void OnLeaveDeepForestEvent(LeaveDeepForestEvent ctx)
        {
            RenderSettings.fogDensity = standardFogDensity;
        }
    }
}