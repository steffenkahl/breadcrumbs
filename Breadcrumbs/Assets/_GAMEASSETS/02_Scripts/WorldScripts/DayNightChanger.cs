﻿using System;
using Breadcrumbs.Events;
using Breadcrumbs.Events.OtherEvents;
using DG.Tweening;
using UnityEngine;

namespace _GAMEASSETS.Scripts.WorldScripts
{
    public class DayNightChanger : MonoBehaviour
    {
        [SerializeField] private Transform sunObject;
        [Range(0f,24f)][SerializeField] private float currentDayTime;
        [SerializeField] private Light sunLight;
        [SerializeField] private Light moonLight;
        [SerializeField] private AnimationCurve sunIntensity;
        [SerializeField] private AnimationCurve fogAmount;
        [SerializeField] private AnimationCurve lightColorRed;
        [SerializeField] private AnimationCurve lightColorGreen;
        [SerializeField] private AnimationCurve lightColorBlue;
        [SerializeField] private Material daySkybox;
        [SerializeField] private Material eveningSkybox;
        [SerializeField] private Material nightSkybox;
        
        private void OnEnable()
        {
            Message<ChangeTimeEvent>.Add(OnChangeTimeEvent);
        }

        private void OnDisable()
        {
            Message<ChangeTimeEvent>.Remove(OnChangeTimeEvent);
        }

        private void OnValidate()
        {
            ChangeTime();
        }

        private void OnChangeTimeEvent(ChangeTimeEvent ctx)
        {
            DoChangeTime(ctx.NewTime);
        }

        public void DoChangeTime(float newTime)
        {
            currentDayTime = newTime;
            ChangeTime();
        }

        private void ChangeTime()
        {
            if(currentDayTime < 6 || currentDayTime> 22)
            {
                //NIGHT
                RenderSettings.skybox = nightSkybox;
            } 
            else if (currentDayTime < 8 || currentDayTime> 18)
            {
                //EVENING
                RenderSettings.skybox = eveningSkybox;
            }
            else
            {
                //DAY
                RenderSettings.skybox = daySkybox;
            }
            
            sunLight.intensity = sunIntensity.Evaluate(currentDayTime);
            Color newColor = new Color(lightColorRed.Evaluate(currentDayTime), lightColorGreen.Evaluate(currentDayTime),
                lightColorBlue.Evaluate(currentDayTime));
            sunLight.color = newColor;
            RenderSettings.fogDensity = fogAmount.Evaluate(currentDayTime);
            
            float xRotation = Mathf.Lerp(-180f,180f,currentDayTime/24f);
            sunObject.localRotation = Quaternion.Euler(new Vector3(xRotation,0,0));
        }
    }
}