using System.Collections;
using System.Collections.Generic;
using Breadcrumbs;
using UnityEngine;
using UnityEngine.Events;

namespace Breadcrumbs
{
    public class Fireplace : MonoBehaviour
    {
        [SerializeField] private int logsNeeded;
        [SerializeField] private int currentLogs;
        [SerializeField] private GameObject[] logs;
        public UnityEvent onCompletingFireplace;

        public void increaseLogs()
        {
            currentLogs += 1;
            
            for (int i = 0; i < currentLogs; i++)
            {
                logs[i].SetActive(true);
            }
            
            if (currentLogs >= logsNeeded)
            {
                onCompletingFireplace.Invoke();
            }
        }
    }
}